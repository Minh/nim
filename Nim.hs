import System.IO
import Data.Char
import Data.Bits

--type
type Board = [Int]
type Players = [String]

--actual nim game Player vs Player and Player vs AI
nimGame :: IO()
nimGame = do
	putStrLn "Welcome to Normal Nim"
	aiChoice <- choice "Play with AI?"
	bool <- choice "Do you want to make the first move?"
	if aiChoice == True
		then takeTurns ["Player 1","AI"] [2,3,4] bool aiChoice
	else takeTurns ["Player 1","Player 2"] [2,3,4] bool aiChoice

--component of the Nim game
--display Board
displayBoard :: Board -> IO()
displayBoard [] = putStr "\n"
displayBoard board = do 
		        putStrLn ((show (length board)) ++ ": " ++ (replicate (last board) '|'))
			displayBoard (init board)


--check number in range or out range
inRange :: Int -> Int -> Int -> Bool
inRange number min max = if (number <= max && number >= min)
			        then True
			 else
			        False

--ASCII code in range
assciRange :: Char -> Bool
assciRange char = inRange (ord char) 48 57

--check if it is really a number
chkInt :: String -> Bool
chkInt str = foldr(&&) True (map (\x -> assciRange x) str)


--get number as an input
getInt :: String -> IO Int
getInt msg = do
		putStr msg
		number <- getLine
		let boolean = chkInt number		
		if (boolean == True) 
			then do 
				return (read(number))
		else
			getInt msg

--replace specific element in list function
replace :: Int -> Int -> Board -> Board
replace index newVal (x:xs)
			| index == 0 = (newVal:xs)
			| otherwise = x:replace (index-1) newVal xs

--calculate object remaining in the heap
cal :: Int -> Int -> IO Int
cal  oldVal objnum  = return (oldVal - objnum)

--remove object from heap
objrem :: String -> Board -> Int -> IO Board
objrem msg board index = do
				removeVal <- getInt msg
				let bool = inRange removeVal 1 (board!!(index-1))
				if bool == True
					then do
						newVal <- cal (board!!(index-1)) removeVal
						let newBoard = replace (index-1) newVal board
						return newBoard
				else objrem "Number out of range, please re-enter: " board index

--take object from heap
takeHeap :: String -> Board -> IO Board
takeHeap msg board = do
			heapIndex <- getInt msg
			let bool = heapChk board (heapIndex-1)
			if bool == True
				then do
					newBoard <- objrem "Number of object to be removed: " board heapIndex
					return newBoard
			else takeHeap "Error, enter the Heap index again: " board
--check the if index exists in heap array
heapChk :: Board -> Int -> Bool
heapChk board index = do
			let bool = inRange index 0 (length board)	
			if ((bool == True) && (board!!index > 0)) then True else False

-- check if board is empty
empty :: Board -> Bool
empty board = if sum board == 0 then True else False

--turn process of a player
playerTurn :: String -> Board -> IO Board
playerTurn player board = do
			putStr (player ++ "'s Turn:\n")
			putStrLn "Board status:"
			displayBoard board
			newBoard <- takeHeap "Choose Heap: " board
			return newBoard

--full game, take in players name, board, and if it is against AI or not (aiMode)
takeTurns :: Players -> Board -> Bool -> Bool -> IO ()
takeTurns players board turn aiMode = do
					if aiMode == False
						then do
							player <- changePlayer players turn
							newBoard <- playerTurn player board
							if ((empty newBoard) == True)
								then do
									winner <- changePlayer players turn
									putStrLn ("Winner is: " ++ winner)
							else takeTurns players newBoard (not(turn)) aiMode
					else do
						player <- changePlayer players turn
						if player == "AI"
							then do 
								newBoard <- aiDecision board
								if (empty newBoard == True)
									then do
										winner <- changePlayer players turn
										putStrLn ("Winner is: " ++ winner)
								else takeTurns players newBoard (not(turn)) aiMode
						else do
							newBoard <- playerTurn player board
							if ( (empty newBoard) == True)
								then do
									winner <- changePlayer players turn
									putStrLn ("Winner is: " ++ winner)
							else takeTurns players newBoard (not(turn)) aiMode

--start first or second option for first player
choice :: String -> IO Bool
choice msg = do
		putStrLn (msg ++ "(y/n)")
		answer <- getLine
		if (answer == "Y" || answer == "y")
			then return True
		else if (answer == "N" || answer == "n") 
			then return False
		else choice ("Invalid choice. " ++ msg)
				
--switch player name during a game
changePlayer :: Players -> Bool -> IO String
changePlayer players bool = if bool == True then return (head players) else return (last players)

--main AI component, takes heap and obj from heap with help of the sub AI component below
aiDecision :: Board -> IO Board

aiDecision board = do 
			putStrLn "AI's Turn:"
			putStrLn "Board Status"
			displayBoard board
			xors <- xorAll board
			if(xors /= 0)
				then do
					heap <- aiSelect board xors
					if(sum board > 1)
						then do
							if( (xor (board!!heap) xors) == 0)
								then do 
									newObjVal <- cal (board!!heap) 1
									let newBoard = replace heap newObjVal board
									return newBoard
							else do
								newObjVal <- cal (board!!heap) (xor (board!!heap) xors)
								let newBoard = replace heap newObjVal board
								return newBoard
					else do
						newObjVal <- cal (board!!heap) 1
						let newBoard = replace heap newObjVal board
						return newBoard
			else do
				heap <- aiSelect board xors
				newObjVal <- cal (board!!heap) 1
				let newBoard = replace heap newObjVal board
				return newBoard
			
--AI sub-components

--xor of all heap
xorAll :: Board -> IO Int
xorAll board = return $ foldr(xor) (head(board)) (drop 1 board)


--Ai selects heap with help of xor, if xor of the whole board is 0 then default move is to take 1 obj from any non-zero heap
aiSelect :: Board -> Int -> IO Int
aiSelect board  xors = do
			if(xors /= 0)
				then do
					if( (xor (last board) xors) < (last board) )
						then return ((length board) - 1)
					else aiSelect (init board) xors
			else do
				if( (last board) /= 0)
					then return ((length board) -1)
				else aiSelect (init board) xors
